﻿#include"OrderItem.h"
#include <iostream>

long OrderItem::getMerchandiseId() const
{
	return merchandiseId;
}

void OrderItem::setMerchandiseId(long merchandiseId)
{
	this->merchandiseId = merchandiseId;
}

int OrderItem::getQuantity() const
{
	return quantity;
}

void OrderItem::setQuantity(int quantity)
{
	this->quantity = quantity;
}

void OrderItem::print() const
{
	std::cout << "Id towaru: " << merchandiseId << std::endl;
	std::cout << "Ilosc: " << quantity << std::endl;
}

//Metoda zwiększająca ilość sztuk, na które jest zamówienie
//WYkorzystywana w przypadku, jeśli próbujemy dodać do zamówienia towar, który już się na nim znajduje
void OrderItem::increaseQuantity(int quantity)
{
	this->quantity += quantity;
}
